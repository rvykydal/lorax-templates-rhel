NAME=lorax-templates-rhel
# RHEL version these templates are designed for
RHELVER=9.0
# Serial number for this release
SERIAL=$(shell [ -f SERIAL ] && cat SERIAL || echo 0)

SPEC=${NAME}.spec
TARBALL=${NAME}-${RHELVER}-${SERIAL}.tar.gz

tar: ${TARBALL}

${TARBALL}: 80-rhel
	git archive --prefix='${NAME}-${RHELVER}/' HEAD $</ | gzip -c9 > $@

bump: bump-serial

bump-serial:
	echo $$((${SERIAL}+1)) > SERIAL
	git add SERIAL

update-spec:
	sed -r -i \
	    -e 's/^(Release:\s+)[^%]+(%.*)$$/\1${SERIAL}\2/' \
	    -e 's/^(Version:\s+).*$$/\1${RHELVER}/' \
	    -e 's/^(Source0:\s+).*$$/\1${TARBALL}/' \
	    ${SPEC}

release: tar update-spec

.PHONY: tar ${TARBALL} bump-serial update-spec release

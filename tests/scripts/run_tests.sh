#!/bin/bash
set -eux

if [ ! -e /usr/share/lorax/templates.d/80-rhel/ ]; then
    echo "Failed to find lorax-templates-rhel templates in /usr/share/lorax/templates.d/"
    exit 1
fi

# Gather up the list of system repo files and use them for lorax
REPOS=$(for f in /etc/yum.repos.d/*repo; do echo -n "--repo $f "; done)
if [ -z "$REPOS" ]; then
    echo "No system repos found"
    exit 1
fi

# Run lorax using the host's repository configuration file
lorax --product="Red Hat Enterprise Linux" --version=9 --release=9 --volid="RHEL-9-test" \
      $REPOS --isfinal --nomacboot /var/tmp/lorax-rhel9-iso/
